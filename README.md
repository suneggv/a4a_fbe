# A4A SQUAB *(Feb 28 2023 version)*
![PRRUU](prruu.png)

The PRRUU audio processing block, outside of the server. Based on the filter bank equalizer by Löllman e Vary **[1]**. In this 28 Feb iteration, it only works with single-ear audiograms and outputs mono audio.

**[1]** - H. W. Löllman, P. Vary - *Generalized filter-bank equalizer for noise reduction with reduced signal delay* - Proc. Interspeech 2005, 2105-2108 *(`DOI: 10.21437/Interspeech.2005-686`)*

## Contents
* Version using input mp3 files _(located in `SQUAB_files`)_
* Version using microphone input _(located in `SQUAB_mic`)_
* Description of the variables used _(variables.xlsx)_

## Requirements
* portaudio 19.6.0-1.2 or later _(available on ubuntu 22.10)_
* pyaudio
* pydub
* numpy
* matplotlib *(optional)*

## Running

### Running SQUAB with mp3 files
SQUAB expects stereo mp3 files. If you give it another format it will freak out, whereas if you give it a mono mp3 file it will probably garble it.

* **Basic usage**
    * `$ cd SQUAB_files/`
    * `$ python SQUAB_with_files.py`
* **Changing input file**
    * In `SQUAB_files/SQUAB_with_files.py`, go to line 47, change the value of `addr__file_name` to the desired one.
* **Changing output filename**
    * In `SQUAB_files/SQUAB_with_files.py`, go to line 476, change the value of `audio_title` to the desired one.
* **Adjusting audiogram**
    * In `SQUAB_files/freqShapDat`, edit the `A_k.txt ` file. SQUAB expects eight positive auditory thresholds, all in a single row, separated by tabs. The frequencies of the auditory thresholds are 0.25, 0.5, 1, 2, 3, 4, 6 and 8 kHz.

### Running SQUAB with microphone

* **Basic usage**
    * `$ cd SQUAB_mic/`
    * `$ python demo_SQUAB_mic_main.py`
* **Adjusting audiogram**
    * In `SQUAB_mic/freqShapDat`, edit the `A_k.txt ` file. SQUAB expects eight positive auditory thresholds, all in a single row, separated by tabs. The frequencies of the auditory thresholds are 0.25, 0.5, 1, 2, 3, 4, 6 and 8 kHz.
* **Toggling plot**
    * In `SQUAB_mic/FBEmodules/debugging_and_visualization.py`, if `VISUALIZATION = True`, a plot of the spectral gain factors will be displayed. If `VISUALIZATION = False`, no such plot will be displayed.