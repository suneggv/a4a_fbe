# ==============================================================================
# ==  2.1 IMPORTS  =============================================================
# ==============================================================================

# ~~ module access from main ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
sys.path.append('./FBEmodules')


# -- audio manipulation --------------------------------------------------------
import numpy as np
import math
from pydub import AudioSegment


# -- record / playback ---------------------------------------------------------
import pyaudio
import time


# -- DCT -----------------------------------------------------------------------
import scipy as sp


# -- sidechain -----------------------------------------------------------------
import threading


# -- visualization -------------------------------------------------------------
from debugging_and_visualization import VISUALIZATION
import matplotlib.pyplot as plt 
import scipy.signal as signal
from   scipy.fft import fft, fftfreq, fftshift, dct
import multiprocessing as mp


# -- debugging -----------------------------------------------------------------
from debugging_and_visualization import DEBUGGING
import numbers


# -- copying nparrays ----------------------------------------------------------
import copy


import types

# ==============================================================================
# ==  FUNCTION IMPORTS  ========================================================
# ==============================================================================

# == imports -- high ============================================================

# -- _______ -- ___ -- Pyaudio interface: bytes --> audio conversion -  -  -  - 
from fbe_high_level_functions import nparray_LR_2_nparrays_L_and_R
from fbe_high_level_functions import nparray_LR_2_nparray_L
from fbe_high_level_functions import bytes_stereo_2_nparray_mono

# -- _______ -- ___ -- Pyaudio interface: audio --> bytes conversion -  -  -  - 
from fbe_high_level_functions import nparray_correct_byteorder
from fbe_high_level_functions import nparray_mono_2_bytearray_stereo

# -- _______ -- ___ -- Import audio -  -  -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import import_audio # y(n)

# -- _______ -- ___ -- Constant FVV calculation -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import hann_window        # win_L(l)
from fbe_high_level_functions import prototype_function # h(l)

# -- _______ -- ___ -- Constant FVM calculation -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import modulation_sequence # varphi(i,l)

# -- _______ -- ___ -- Principal function -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import main_function # v(n)


# == imports -- mid ============================================================

# -- _______ -- ___ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import get_beta_now # beta_now(l)

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import AFB_sidechain # w_l^now for n^now

# -- _______ -- ____ -- Principal function -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import RT_plot # Real-time plotting


# == imports -- sunk ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import get_Y_now   # Y_i_now
from fbe_sunk_level_functions import update_Ymat # Y_mat, Y_1, Y_S, Y_L
from fbe_sunk_level_functions import get_W_now   # W_i_now
from fbe_sunk_level_functions import get_w_now   # w_l_now

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import update_stats # P_i,E(P_i),E(P_i^2),Var(P_i)


# == imports -- yawn ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_yawn_level_functions import get_P               # P_i
from fbe_yawn_level_functions import get_avgP            # E(P_i)
from fbe_yawn_level_functions import get_avgP2           # E(P_i^2)
from fbe_yawn_level_functions import get_varP            # Var(P_i)
from fbe_yawn_level_functions import spect_lin_to_dB_SPL # W_i_now
from fbe_yawn_level_functions import get_W_ABS           # W_i_now
from fbe_yawn_level_functions import get_W_FSh           # W_i_now

# == imports -- debug ==========================================================

from debug_funcs import PrintException



# == FBE manager ===============================================================

def mike_callback( mike_queue  
                 ):

    if DEBUGGING:
        assert type(mike_queue) == bytearray

    def callback( in_data,     # recorded data
                  frame_count, # number of frames
                  time_info,   # dictionary
                  status       # PaCallbackFlags
                ):
        if DEBUGGING:
            assert type(in_data)     == bytes
            assert type(frame_count) == int
            assert type(time_info)   == dict
            assert type(status)      == int

        mike_queue.extend(in_data)

        out = (in_data, pyaudio.paContinue)
        if DEBUGGING:
            assert type(out)    == tuple
            assert type(out[0]) == bytes
            assert type(out[1]) == int
        return out

    if DEBUGGING:
        assert type(callback) == types.FunctionType
    return callback  


def mike_queue_manager( pls_stop_mqm,
                        mike_queue,
                        boxed_vs__y__int,
                        pm__R__sam,
                        chunk          = 1024,
                        sample_format  = pyaudio.paInt16, 
                        channels       = 2, # 1 --> mono, 2 --> stereo
                        fs             = 44100,
                        sleep_fraction = 0.2
                      ):

    if DEBUGGING:
        assert       type(pls_stop_mqm)        == threading.Event
        assert       type(mike_queue)          == bytearray
        assert       type(boxed_vs__y__int)    == list
        assert        len(boxed_vs__y__int)    == 1
        assert       type(boxed_vs__y__int[0]) == np.ndarray
        assert isinstance(boxed_vs__y__int[0][0], numbers.Number)
        assert       type(pm__R__sam)          == int
        assert       type(chunk)               == int
        assert       type(sample_format)       == int
        assert       type(channels)            == int
        assert       type(fs)                  == int
        assert       type(sleep_fraction)      == float

    sample_size        = pyaudio.get_sample_size(sample_format)
    R_samples_in_queue = pm__R__sam * channels * sample_size

    while not pls_stop_mqm.wait(0):
        try:
            if len(mike_queue) > R_samples_in_queue:

                y_new =           bytes_stereo_2_nparray_mono( 
                        bytes(mike_queue[:R_samples_in_queue]),
                                                         chunk,
                                                 sample_format,
                                                      channels,
                                                            fs)
                boxed_vs__y__int[0] = np.append(boxed_vs__y__int[0], y_new)
                del mike_queue[:R_samples_in_queue]

            else:
                time.sleep( sleep_fraction * pm__R__sam / fs ) # wait for audio
        except:
            PrintException()
            break
    
    print("MQM: stopped successfully")        
    return


def line_callback( line_queue,
                   chunk,
                   channels,
                   sample_format
                 ):

    if DEBUGGING:
        assert type(line_queue)    == bytearray
        assert type(chunk)         == int
        assert type(channels)      == int
        assert type(sample_format) == int

    def callback( in_data,     # recorded data (ie this is None bc input=False)
                  frame_count, # number of frames
                  time_info,   # dictionary
                  status       # PaCallbackFlags
                ):
        if DEBUGGING:
            assert type(in_data)     == types.NoneType
            assert type(frame_count) == int
            assert type(time_info)   == dict
            assert type(status)      == int

        out_data = bytearray()
        sz = chunk * channels * pyaudio.get_sample_size(sample_format)

        if len(line_queue) >= sz:
            out_data = line_queue[:sz]
            del line_queue[:sz]

        else:
            out_data = bytes(sz)

        out = (bytes(out_data), pyaudio.paContinue)
        if DEBUGGING:
            assert type(out)    == tuple
            assert type(out[0]) == bytes
            assert type(out[1]) == int
        return out

    if DEBUGGING:
        assert type(callback) == types.FunctionType
    return callback


def line_queue_manager( pls_stop_lqm,
                        line_queue,
                        boxed_vs__v__int,
                        pm__R__sam,
                        sample_format  = pyaudio.paInt16, 
                        channels       = 2, # 1 --> mono, 2 --> stereo
                        fs             = 44100,
                        sleep_fraction = 0.2
                      ):

    if DEBUGGING:
        assert       type(pls_stop_lqm)        == threading.Event
        assert       type(line_queue)          == bytearray
        assert       type(boxed_vs__v__int)    == list
        assert        len(boxed_vs__v__int)    == 1
        assert       type(boxed_vs__v__int[0]) == np.ndarray
        assert isinstance(boxed_vs__v__int[0][0], numbers.Number)
        assert       type(pm__R__sam)          == int
        assert       type(sample_format)       == int
        assert       type(channels)            == int
        assert       type(fs)                  == int
        assert       type(sleep_fraction)      == float

    sample_size        = pyaudio.get_sample_size(sample_format)
    R_samples_in_queue = pm__R__sam * channels * sample_size

    while not pls_stop_lqm.wait(0):
        try:
            if len(boxed_vs__v__int[0]) > pm__R__sam:

                v_new           = boxed_vs__v__int[0][:pm__R__sam]
                v_new_bytearray = nparray_mono_2_bytearray_stereo( v_new,
                                                                   sample_format
                                                                 )
                line_queue.extend(v_new_bytearray)
                boxed_vs__v__int[0] = boxed_vs__v__int[0][pm__R__sam:]

            else:
                time.sleep( sleep_fraction * pm__R__sam / fs ) # wait for audio
        except:
                PrintException()
                break
    
    print("LQM: stopped successfully")        
    return


def FBE_with_microphone( pls_stop_fbe,
                         boxed_vs__y__int,
                         boxed_vs__v__int,
                         addr__A_k,
                         addr__T_i_nml,
                         addr__C_ik,
                         addr__W_ABP,
                         pm__chunk__sam         = 1024,
                         pm__sample_format__int = pyaudio.paInt16,
                         pm__channels__int      = 2,
                         pm__fs__Hz             = 44100,
                         pm__M__int             = 64,
                         pm__L__int             = 64,
                         pm__R__sam             = 441,
                         pm__BS__int            = 5,
                         pm__BL__int            = 200,
                         pm__decay__fct         = 0.99,
                         pm__P_0__fct           = 1.1082721367868039e-07
                       ):

    if DEBUGGING:
        assert       type(pls_stop_fbe)           == threading.Event
        assert       type(boxed_vs__y__int)       == list
        assert        len(boxed_vs__y__int)       == 1
        assert       type(boxed_vs__y__int[0])    == np.ndarray
        assert isinstance(boxed_vs__y__int[0][0],    numbers.Number)
        assert       type(boxed_vs__v__int)       == list
        assert        len(boxed_vs__v__int)       == 1
        assert       type(boxed_vs__v__int[0])    == np.ndarray
        assert isinstance(boxed_vs__v__int[0][0],    numbers.Number)
        assert       type(addr__A_k)              == str
        assert       type(addr__T_i_nml)          == str
        assert       type(addr__C_ik)             == str
        assert       type(addr__W_ABP)            == str
        assert       type(pm__chunk__sam)         == int
        assert       type(pm__sample_format__int) == int
        assert       type(pm__channels__int)      == int
        assert       type(pm__fs__Hz)             == int
        assert       type(pm__M__int)             == int
        assert       type(pm__L__int)             == int
        assert       type(pm__R__sam)             == int
        assert       type(pm__BS__int)            == int
        assert       type(pm__BL__int)            == int
        assert       type(pm__decay__fct)         == float
        assert       type(pm__P_0__fct)           == float

    # 2. IMPORTS ---------------------------------------------------------------

    # -- 2.1. Import modules - - - - - - - - - - - - - - - - - - - - - - - - - -

    # see above

    # -- 2.2. Import audio - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # N/A

    # -- 2.3. Import frequency shaper data - - - - - - - - - - - - - - - - - - -

    vv__A_k__dBH     = np.loadtxt(addr__A_k)
    vv__T_i_nml__dBS = np.loadtxt(addr__T_i_nml)
    mf__C_ik__fct    = np.loadtxt(addr__C_ik)
    vv__W_ABP__fct   = np.loadtxt(addr__W_ABP)


    # 3. Settings, definitions and instantiations ------------------------------

    # ---- 3.1.1. Set parameter values -  -  -  -  -  -  -  -  -  -  -  -  -  - 

    # see above

    # ---- 3.1.2. Set signal value vectors -  -  -  -  -  -  -  -  -  -  -  -  -

    # N/A

    # ---- 3.2.1. Define axes and coords -  -  -  -  -  -  -  -  -  -  -  -  -  

    # ------ 3.2.1a. axes -   -   -   -   -   -   -   -   -   -   -   -   -   - 

    va__l__idx = np.array(range(pm__L__int + 1))
    va__i__idx = np.array(range(pm__M__int))
    va__k__idx = np.array(range(8))

    # ------ 3.2.1b. coords -   -   -   -   -   -   -   -   -   -   -   -   -   

    pc__n_now__idx = 0

    # ---- 3.3.1. Instantiate SVVs, FVVs and FVMs -  -  -  -  -  -  -  -  -  -  

    # ------ 3.3.1a. single value vectors (SVVs) -   -   -   -   -   -   -   -  

    vs__beta__int = np.zeros(len(va__l__idx)).astype('int16')


    # ------ 3.3.1b. function value vectors (FVVs) -   -   -   -   -   -   -   -

    vf__win__flt = np.zeros(len(va__l__idx))
    vf__h__flt   = np.zeros(len(va__l__idx))

    vf__Y__flt  = np.zeros(len(va__i__idx)).astype("complex128")
    vf__Y1__flt = np.zeros(len(va__i__idx)).astype("float64")
    vf__YS__flt = np.zeros(len(va__i__idx)).astype("float64")
    vf__YL__flt = np.zeros(len(va__i__idx)).astype("float64")

    vv__W_ABS__fct = np.zeros(len(va__i__idx))
    vf__W__fct     = np.zeros(len(va__i__idx))
    vf__w__flt     = np.zeros(len(va__l__idx))

    vf__P__flt     = np.zeros(len(va__i__idx))
    vf__avgP__flt  = np.zeros(len(va__i__idx))
    vf__avgP2__flt = np.zeros(len(va__i__idx))
    vf__varP__flt  = np.zeros(len(va__i__idx))

    vf__HL_i__dB_        = np.zeros(len(va__i__idx))
    vf__a_i__rad         = np.zeros(len(va__i__idx))
    vf__1_cot_a_i__fct   = np.zeros(len(va__i__idx))
    vf__1_cot_a_i_6__fct = np.zeros(len(va__i__idx))
    vf__HL_cot_a_i__dB_  = np.zeros(len(va__i__idx))


    # ------ 3.3.1c. function value matrices (FVMs) -   -   -   -   -   -   -   

    mf__varphi__xfl = np.zeros((len(va__i__idx),
                                len(va__l__idx))).astype("complex128")

    mf__h_varphi__xfl = np.zeros((len(va__i__idx),
                                  len(va__l__idx))).astype("complex128")

    mf__Y__flt = np.zeros( (len(va__i__idx), pm__BL__int) ).astype("float64")


    # 4. Function value thing salad calculation --------------------------------

    # ---- 4.1.1. Constant function value vector calculation -  -  -  -  -  -  -

    # ~~~~~~ 4.1.1a. win_L(l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for l in va__l__idx:
        vf__win__flt[l] = hann_window(l, pm__L__int)


    # ~~~~~~ 4.1.1b. h(l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for l in va__l__idx:
        vf__h__flt[l] = prototype_function(l, 
                                           vf__win__flt, 
                                           pm__L__int, 
                                           pm__M__int)

        
    # ~~~~~~ 4.1.1c. HL_i ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for i in va__i__idx:
        vf__HL_i__dB_[i] = np.dot(vv__A_k__dBH, mf__C_ik__fct[i,:])


    # ~~~~~~ 4.1.1d. alpha_i ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    for i in va__i__idx:
        vf__a_i__rad[i] = (47.0 + 0.45 * vf__HL_i__dB_[i]) / 57.295779513


    # ~~~~~~ 4.1.1e. 1 - cot(alpha_i) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for i in va__i__idx:
        vf__1_cot_a_i__fct[i] = 1.0 - 1.0/np.tan(vf__a_i__rad[i])

        
    # ~~~~~~ 4.1.1f. (1 - cot(alpha_i)) / 6 ~   ~   ~   ~   ~   ~   ~   ~   ~   
    for i in va__i__idx:
        vf__1_cot_a_i_6__fct[i] = (1.0/6.0) * vf__1_cot_a_i__fct[i]
        

    # ~~~~~~ 4.1.1g. HL_i/(1 - cot(alpha_i)) ~   ~   ~   ~   ~   ~   ~   ~   ~  
    for i in va__i__idx:
        vf__HL_cot_a_i__dB_[i] = vf__HL_i__dB_[i]/vf__1_cot_a_i__fct[i]


    # ---- 4.1.2. Constant function value matrix calculation -  -  -  -  -  -  -

    # ~~~~~~ 4.1.2a. phi(i,l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for i in va__i__idx:
        for l in va__l__idx:
            mf__varphi__xfl[i,l] = modulation_sequence(i,l,pm__M__int)


    # ~~~~~~ 4.1.2b. h(l)phi(i,l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    mf__h_ext__flt    = np.outer(np.ones(len(va__i__idx)), 
                                 vf__h__flt).astype(vf__h__flt.dtype)
    mf__h_varphi__xfl = mf__h_ext__flt * mf__varphi__xfl


    # ---- 4.2.1. Variable FVV/M calculation -  -  -  -  -  -  -  -  -  -  -  - 

    # ~~~~~~ 4.2.1a. beta_now(l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    vs__beta__int = get_beta_now(np.zeros(len(va__l__idx)),
                                 pc__n_now__idx,
                                 va__l__idx
                                )


    # ~~~~~~ 4.2.1b. Y_i_now ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    vf__Y__flt = get_Y_now(vs__beta__int,
                           mf__h_varphi__xfl,
                           va__i__idx,
                           va__l__idx
                          )


    # ~~~~~~ 4.2.1c. Y_mat, Y_1, Y_S, Y_L ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    (mf__Y__flt, 
     vf__Y1__flt,
     vf__YS__flt, 
     vf__YL__flt) = update_Ymat(Y_now  = vf__Y__flt,
                                i_axis = va__i__idx,
                                Y_mat  = mf__Y__flt,
                                Y_1    = vf__Y1__flt,
                                Y_S    = vf__YS__flt,
                                Y_L    = vf__YL__flt,
                                B_S    = pm__BS__int,
                                B_L    = pm__BL__int
                               )


    # ~~~~~~ 4.2.1d. P_i ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    vf__P__flt = get_P(P_i = vf__P__flt,
                       Y_1 = vf__Y1__flt,
                       Y_S = vf__YS__flt,
                       B_S = pm__BS__int
                      )


    # ~~~~~~ 4.2.1e. E(P_i) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   
    vf__avgP__flt = get_avgP(EP_i = vf__avgP__flt,
                             Y_1  = vf__Y1__flt,
                             Y_L  = vf__YL__flt,
                             B_L  = pm__BL__int
                            )


    # ~~~~~~ 4.2.1f. E(P_i^2) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    vf__avgP2__flt = get_avgP2(EP2_i = vf__avgP2__flt,
                               Y_1   = vf__Y1__flt,
                               Y_L   = vf__YL__flt,
                               B_L   = pm__BL__int
                              )


    # ~~~~~~ 4.2.1g. Var(P_i^2) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   
    vf__varP__flt = get_varP(EP_i  = vf__avgP__flt,
                             EP2_i = vf__avgP2__flt
                            )


    # ~~~~~~ 4.2.1h. W_i_now ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    (vv__W_ABS__fct,
     vf__W__fct)     = get_W_now(W_ABP       = vv__W_ABP__fct,
                                 W_ABS       = vv__W_ABS__fct,
                                 i_cot_a_i_6 = vf__1_cot_a_i_6__fct,
                                 HL_cot_a_i  = vf__HL_cot_a_i__dB_,
                                 T_i_nml     = vv__T_i_nml__dBS,
                                 P_i         = vf__P__flt,
                                 EP_i        = vf__avgP__flt,
                                 varP_i      = vf__varP__flt,
                                 i_axis      = va__i__idx,
                                 D           = pm__decay__fct,
                                 P_0         = pm__P_0__fct
                                )


    # ~~~~~~ 4.2.1i. w_l_now ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    vf__w__flt = get_w_now(vf__w__flt,
                           va__l__idx,
                           vf__W__fct,
                           va__i__idx,
                           pm__M__int,
                           pm__L__int
                          )


    # 5. Analysis filter bank --------------------------------------------------

    # ~~ 5a. Update P_i, E(P_i), E(P_i^2) and Var(P_i) ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

    # used by the principal function

    # ~~ 5b. Update the value of w_l^now for n^now ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

    # used by the principal function


    # 6. Principal function ----------------------------------------------------

    try:
        main_function(pls_stop_fbe     = pls_stop_fbe,
                      boxed_vs__y__int = boxed_vs__y__int,
                      boxed_vs__v__int = boxed_vs__v__int,
                      monad_beta_l     = vs__beta__int,
                      h_l              = vf__h__flt,
                      monad_w_l        = vf__w__flt,
                      monad_W_i        = vf__W__fct,
                      monad_Y_i        = vf__Y__flt,
                      h_phi            = mf__h_varphi__xfl,
                      i_axis           = va__i__idx,
                      l_axis           = va__l__idx,
                      monad_n_now      = pc__n_now__idx,
                      monad_Y_mat      = mf__Y__flt,
                      monad_Y_1        = vf__Y1__flt,
                      monad_Y_S        = vf__YS__flt,
                      monad_Y_L        = vf__YL__flt,
                      monad_P_i        = vf__P__flt,
                      monad_EP_i       = vf__avgP__flt,
                      monad_EP2_i      = vf__avgP2__flt,
                      monad_varP_i     = vf__varP__flt,
                      i_cot_a_i_6      = vf__1_cot_a_i_6__fct,
                      HL_cot_a_i       = vf__HL_cot_a_i__dB_,
                      T_i_nml          = vv__T_i_nml__dBS,
                      monad_W_ABS      = vv__W_ABS__fct,
                      W_ABP            = vv__W_ABP__fct,
                      D                = pm__decay__fct,
                      P_0              = pm__P_0__fct,
                      M                = pm__M__int,
                      L                = pm__L__int,
                      R                = pm__R__sam,
                      B_S              = pm__BS__int,
                      B_L              = pm__BL__int,
                      PLS_GIVE_PLOTS   = True
                     )
    except:
        PrintException()
    
    print("FBE_with_microphone: successfully returning")
    return



def FBE_manager( addr__A_k,
                 addr__T_i_nml,
                 addr__C_ik,
                 addr__W_ABP,
                 pm__chunk__sam         = 1024,
                 pm__sample_format__int = pyaudio.paInt16,
                 pm__channels__int      = 2,
                 pm__fs__Hz             = 44100,
                 pm__M__int             = 64,
                 pm__L__int             = 64,
                 pm__R__sam             = 441,
                 pm__BS__int            = 5,
                 pm__BL__int            = 200,
                 pm__decay__fct         = 0.99,
                 pm__P_0__fct           = 1.1082721367868039e-07,
                 sleep_fraction         = 0.2
               ):

    if DEBUGGING:
        assert type(addr__A_k)              == str
        assert type(addr__T_i_nml)          == str
        assert type(addr__C_ik)             == str
        assert type(addr__W_ABP)            == str
        assert type(pm__chunk__sam)         == int
        assert type(pm__sample_format__int) == int
        assert type(pm__channels__int)      == int
        assert type(pm__fs__Hz)             == int
        assert type(pm__M__int)             == int
        assert type(pm__L__int)             == int
        assert type(pm__R__sam)             == int
        assert type(pm__BS__int)            == int
        assert type(pm__BL__int)            == int
        assert type(pm__decay__fct)         == float
        assert type(pm__P_0__fct)           == float
        assert type(sleep_fraction)         == float

    pa            = pyaudio.PyAudio() 
    chunk         = pm__chunk__sam
    sample_format = pm__sample_format__int
    channels      = pm__channels__int
    fs            = pm__fs__Hz

    # -- 1. Instantiate FBE-shared variables -----------------------------------

    boxed_vs__y__int = [np.array([0]).astype('int16')] # this is to prevent
    boxed_vs__v__int = [np.array([0]).astype('int16')] # initialization errors

    # -- 2. Prepare mike audio environment -------------------------------------

    print("MGR: setting up mike audio environment")
    mike_queue  = bytearray()
    mike_stream = pa.open( format            = sample_format,
                           channels          = channels,
                           rate              = fs,
                           input             = True,
                           frames_per_buffer = chunk,
                           stream_callback   = mike_callback( mike_queue )
                         )

    pls_stop_mqm = threading.Event()
    mqm_thread   = threading.Thread( target = mike_queue_manager,
                                     args   = ( pls_stop_mqm,
                                                mike_queue,
                                                boxed_vs__y__int,
                                                pm__R__sam,
                                                chunk,
                                                sample_format, 
                                                channels,
                                                fs,
                                                sleep_fraction
                                              )
                                   )

    # -- 3. Prepare line audio environment -------------------------------------

    print("MGR: setting up line audio environment")
    line_queue  = bytearray()
    line_stream = pa.open( format            = sample_format,
                           channels          = channels,
                           rate              = fs,
                           output            = True,
                           frames_per_buffer = chunk,
                           stream_callback   = line_callback( line_queue,
                                                              chunk,
                                                              channels,
                                                              sample_format
                                                            )
                         )

    pls_stop_lqm = threading.Event()
    lqm_thread   = threading.Thread( target = line_queue_manager,
                                     args   = ( pls_stop_lqm,
                                                line_queue,
                                                boxed_vs__v__int,
                                                pm__R__sam,
                                                sample_format, 
                                                channels,
                                                fs,
                                                sleep_fraction
                                              )
                                   )

    # -- 4. Prepare FBE --------------------------------------------------------

    print("MGR: preparing FBE")
    pls_stop_fbe = threading.Event()
    fbe_thread   = threading.Thread( target = FBE_with_microphone,
                                     args   = ( pls_stop_fbe,
                                                boxed_vs__y__int,
                                                boxed_vs__v__int,
                                                addr__A_k,
                                                addr__T_i_nml,
                                                addr__C_ik,
                                                addr__W_ABP,
                                                pm__chunk__sam,
                                                pm__sample_format__int,
                                                pm__channels__int,
                                                pm__fs__Hz,
                                                pm__M__int,
                                                pm__L__int,
                                                pm__R__sam,
                                                pm__BS__int,
                                                pm__BL__int,
                                                pm__decay__fct,
                                                pm__P_0__fct
                                              )
                                   )

    # -- 5. Start all threads --------------------------------------------------

    print("MGR: starting threads")
    mqm_thread.start()
    lqm_thread.start()
    fbe_thread.start()

    # -- 6. Keep running until I want it to stop -------------------------------

    print("MGR: running. Press Ctrl + c to stop")
    while mike_stream.is_active():
        try:
            if DEBUGGING:
                assert line_stream.is_active()
                assert mqm_thread.is_alive()
                assert lqm_thread.is_alive()
                assert fbe_thread.is_alive()
            time.sleep(0.2)
        except KeyboardInterrupt:
            print("\nMGR: stopping...")
            mike_stream.stop_stream()
            break

    # -- 7. Safely stop all threads --------------------------------------------

    while mike_stream.is_active():
        time.sleep(0.1)
    print("MGR: mike stream stopped. Stopping mike queue manager")
    pls_stop_mqm.set()

    while mqm_thread.is_alive():
        time.sleep(0.1)
    print("MGR: mike queue manager stopped. Stopping FBE")
    pls_stop_fbe.set()

    while fbe_thread.is_alive():
        time.sleep(0.1)
    print("MGR: FBE stopped. Stopping line queue manager")
    pls_stop_lqm.set()

    while lqm_thread.is_alive():
        time.sleep(0.1)
    print("MGR: line queue manager stopped. Stopping line stream")
    line_stream.stop_stream()

    while mike_stream.is_active():
        time.sleep(0.1)
    print("MGR: line stream stopped. Terminating streams and threads")

    # -- 8. Terminate streams and threads --------------------------------------

    pa.terminate()
    mqm_thread.join()
    lqm_thread.join()
    fbe_thread.join()
    print("MGR: done!")
    return





