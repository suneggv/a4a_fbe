from module_imports import *

# == imports -- sunk ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import get_Y_now   # Y_i_now
from fbe_sunk_level_functions import update_Ymat # Y_mat, Y_1, Y_S, Y_L
from fbe_sunk_level_functions import get_W_now   # W_i_now
from fbe_sunk_level_functions import get_w_now   # w_l_now

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import update_stats # P_i,E(P_i),E(P_i^2),Var(P_i)


# == imports -- yawn ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_yawn_level_functions import get_P               # P_i
from fbe_yawn_level_functions import get_avgP            # E(P_i)
from fbe_yawn_level_functions import get_avgP2           # E(P_i^2)
from fbe_yawn_level_functions import get_varP            # Var(P_i)
from fbe_yawn_level_functions import spect_lin_to_dB_SPL # W_i_now
from fbe_yawn_level_functions import get_W_ABS           # W_i_now
from fbe_yawn_level_functions import get_W_FSh           # W_i_now



# ~~ 2.2 Import audio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# see fbe_high_level_functions.py



# ~~ 4.1.1. Constant function value vector calculation ~~~~~~~~~~~~~~~~~~~~~~~~~
# see fbe_high_level_functions.py



# ~~ 4.1.2. Constant function value matrix calculation ~~~~~~~~~~~~~~~~~~~~~~~~~
# see fbe_high_level_functions.py



# ~~ 4.2.1. Variable function value vector / matrix calculation ~~~~~~~~~~~~~~~~

# ---- 4.2.1a. beta_now(l) -----------------------------------------------------

def get_beta_now(y,
                 n_now,
                 l_axis
                ):

    if DEBUGGING:
        assert              type(y)        == np.ndarray
        assert        isinstance(y[0],        numbers.Number)
        assert ( issubclass(type(n_now)     , np.integer) or
                            type(n_now)    == int)
        assert              type(l_axis)   == np.ndarray
        assert   issubclass(type(l_axis[0]) , np.integer)
    
    the_zeros = np.array([]).astype(y.dtype)
    
    if n_now < l_axis[-1]:
        the_zeros = np.zeros(l_axis[-1] - n_now).astype(y.dtype)
    
    the_reversed = np.array(y[max(0, n_now - l_axis[-1]):n_now + 1])[::-1].astype(y.dtype)
    out          = np.concatenate((the_reversed, the_zeros)).astype(y.dtype)

    if DEBUGGING:
        assert     out.dtype == y.dtype
        assert len(out)      == len(l_axis)
    
    return out

# ---- 4.2.1b. Y_i_now ---------------------------------------------------------
# see fbe_sunk_level_functions.py

# ---- 4.2.1c. Y_mat, Y_1, Y_S, Y_L --------------------------------------------
# see fbe_sunk_level_functions.py

# ---- 4.2.1d. P_i -------------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1e. E(P_i) ----------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1f. E(P_i^2) --------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1g. Var(P_i) ------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1h. W_i_now ---------------------------------------------------------
# see fbe_sunk_level_functions.py and fbe_yawn_level_functions.py

# ---- 4.2.1i. w_l_now ---------------------------------------------------------
# see fbe_sunk_level_functions.py



# ~~ 5. Analysis filter bank ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---- 5a. Update the value of P_i, E(P_i), E(P_i^2) and Var(P_i) --------------
# see fbe_sunk_level_functions.py

# ---- 5b. Update the value of w_l^now for n^now -------------------------------

def AFB_sidechain(pls_stop_FB,
                  boxed_n_now,
                  y,
                  h_phi,
                  boxed_beta_l,
                  boxed_Y_i,
                  boxed_W_i,
                  boxed_w_l,
                  i_axis,
                  l_axis,
                  boxed_Y_mat,
                  boxed_Y_1,
                  boxed_Y_S,
                  boxed_Y_L,
                  boxed_P_i,
                  boxed_EP_i,
                  boxed_EP2_i,
                  boxed_varP_i,
                  i_cot_a_i_6,
                  HL_cot_a_i,
                  T_i_nml,
                  boxed_W_ABS,
                  W_ABP,
                  D   = 0.99,
                  P_0 = 1.1082721367868039e-07,
                  M   = 64,
                  L   = 64,
                  B_S = 5,
                  B_L = 200
                 ):
    
    if DEBUGGING:
        assert                type(pls_stop_FB)          == threading.Event
        assert                type(boxed_n_now)          == list
        assert                 len(boxed_n_now)          == 1
        assert   ( issubclass(type(boxed_n_now[0])        , np.integer) or
                              type(boxed_n_now[0])       == int)
        assert                type(y)                    == np.ndarray
        assert          isinstance(y[0],                    numbers.Number)
        assert                type(h_phi)                == np.ndarray
        assert                 len(h_phi.shape)          == 2
        assert     np.iscomplexobj(h_phi)
        assert                type(boxed_beta_l)         == list
        assert                 len(boxed_beta_l)         == 1
        assert                type(boxed_beta_l[0])      == np.ndarray
        assert          isinstance(boxed_beta_l[0][0]     , numbers.Number)
        assert                type(boxed_Y_i)            == list
        assert                 len(boxed_Y_i)            == 1
        assert                type(boxed_Y_i[0])            == np.ndarray
        assert                     boxed_Y_i[0].dtype    == h_phi.dtype
        assert                type(boxed_W_i)            == list
        assert                 len(boxed_W_i)            == 1
        assert                type(boxed_W_i[0])         == np.ndarray
        assert          isinstance(boxed_W_i[0][0]        , numbers.Number)
        assert not issubclass(type(boxed_W_i[0])          , np.integer)
        assert                type(boxed_w_l)            == list
        assert                 len(boxed_w_l)            == 1
        assert                type(boxed_w_l[0])         == np.ndarray
        assert          isinstance(boxed_w_l[0][0]        , numbers.Number)
        assert not issubclass(type(boxed_w_l[0])          , np.integer)
        assert                type(i_axis)               == np.ndarray
        assert     issubclass(type(i_axis[0])             , np.integer)
        assert                type(l_axis)               == np.ndarray
        assert     issubclass(type(l_axis[0])             , np.integer)
        assert                     h_phi.shape[0]        == len(i_axis)
        assert                     h_phi.shape[1]        == len(l_axis)
        assert                 len(boxed_Y_i[0])         == len(i_axis)
        assert                 len(boxed_W_i[0])         == len(i_axis)
        assert                 len(boxed_w_l[0])         == len(l_axis)
        assert                type(boxed_Y_mat)          == list
        assert                 len(boxed_Y_mat)          == 1
        assert                type(boxed_Y_mat[0])       == np.ndarray
        assert                 len(boxed_Y_mat[0].shape) == 2
        assert          isinstance(boxed_Y_mat[0][0,0],     numbers.Number)
        assert not issubclass(type(boxed_Y_mat[0]),         np.integer)
        assert                 len(boxed_Y_mat[0][:,0])  == len(i_axis)
        assert                 len(boxed_Y_mat[0][0,:])  == B_L
        assert                type(boxed_Y_1)            == list
        assert                 len(boxed_Y_1)            == 1
        assert                type(boxed_Y_1[0])         == np.ndarray
        assert          isinstance(boxed_Y_1[0][0],         numbers.Number)
        assert not issubclass(type(boxed_Y_1[0]),           np.integer)
        assert                 len(boxed_Y_1[0])         == len(i_axis)
        assert                type(boxed_Y_S)            == list
        assert                 len(boxed_Y_S)            == 1
        assert                type(boxed_Y_S[0])         == np.ndarray
        assert          isinstance(boxed_Y_S[0][0],         numbers.Number)
        assert not issubclass(type(boxed_Y_S[0]),           np.integer)
        assert                 len(boxed_Y_S[0])         == len(i_axis)
        assert                type(boxed_Y_L)            == list
        assert                 len(boxed_Y_L)            == 1
        assert                type(boxed_Y_L[0])         == np.ndarray
        assert          isinstance(boxed_Y_L[0][0],         numbers.Number)
        assert not issubclass(type(boxed_Y_L[0]),           np.integer)
        assert                 len(boxed_Y_L[0])         == len(i_axis)
        assert                type(boxed_P_i)            == list
        assert                 len(boxed_P_i)            == 1
        assert                type(boxed_P_i[0])         == np.ndarray
        assert          isinstance(boxed_P_i[0][0],         numbers.Number)
        assert not issubclass(type(boxed_P_i[0]),           np.integer)
        assert                 len(boxed_P_i[0])         == len(boxed_Y_1[0])
        assert                 len(boxed_P_i[0])         == len(boxed_Y_S[0])
        assert                type(boxed_EP_i)           == list
        assert                 len(boxed_EP_i)           == 1
        assert                type(boxed_EP_i[0])        == np.ndarray
        assert          isinstance(boxed_EP_i[0][0],        numbers.Number)
        assert not issubclass(type(boxed_EP_i[0]),          np.integer)
        assert                 len(boxed_EP_i[0])        == len(boxed_Y_L[0])
        assert                type(boxed_EP2_i)          == list
        assert                 len(boxed_EP2_i)          == 1
        assert                type(boxed_EP2_i[0])       == np.ndarray
        assert          isinstance(boxed_EP2_i[0][0],       numbers.Number)
        assert not issubclass(type(boxed_EP2_i[0]),         np.integer)
        assert                 len(boxed_EP2_i[0])       == len(boxed_Y_1[0])
        assert                type(boxed_varP_i)         == list
        assert                 len(boxed_varP_i)         == 1
        assert                type(boxed_varP_i[0])      == np.ndarray
        assert          isinstance(boxed_varP_i[0][0],      numbers.Number)
        assert not issubclass(type(boxed_varP_i[0]),        np.integer)
        assert                type(i_cot_a_i_6)          == np.ndarray
        assert          isinstance(i_cot_a_i_6[0],          numbers.Number)
        assert not issubclass(type(i_cot_a_i_6),            np.integer)
        assert                 len(i_cot_a_i_6)          == len(i_axis)
        assert                type(HL_cot_a_i)           == np.ndarray
        assert          isinstance(HL_cot_a_i[0],           numbers.Number)
        assert not issubclass(type(HL_cot_a_i),             np.integer)
        assert                 len(HL_cot_a_i)           == len(i_axis)
        assert                type(T_i_nml)              == np.ndarray
        assert          isinstance(T_i_nml[0],              numbers.Number)
        assert not issubclass(type(T_i_nml),                np.integer)
        assert                 len(T_i_nml)              == len(i_axis)
        assert                type(boxed_W_ABS)          == list
        assert                 len(boxed_W_ABS)          == 1
        assert                type(boxed_W_ABS[0])       == np.ndarray
        assert          isinstance(boxed_W_ABS[0][0],       numbers.Number)
        assert not issubclass(type(boxed_W_ABS[0]),         np.integer)
        assert                type(W_ABP)                == np.ndarray
        assert          isinstance(W_ABP[0],                numbers.Number)
        assert not issubclass(type(W_ABP),                  np.integer)
        assert                 len(W_ABP)                == len(i_axis)
        assert                type(D)                    == float
        assert                type(P_0)                  == float
        assert                type(M)                    == int
        assert                     M % 2                 == 0
        assert                     M                     == L
        assert                type(B_S)                  == int
        assert                type(B_L)                  == int
        assert                     B_S                    < B_L
        
    child_n_now = boxed_n_now[0]
    
    while not pls_stop_FB.wait(0):
        if child_n_now != boxed_n_now[0]: # the value of boxed_n_now
                                          # has been updated
            try:
                child_n_now  = boxed_n_now[0]
                child_beta_l = np.array(boxed_beta_l[0])

                child_Y_i = get_Y_now(child_beta_l,
                                      h_phi,
                                      i_axis,
                                      l_axis
                                     )
                (child_Y_mat, 
                 child_Y_1,
                 child_Y_S, 
                 child_Y_L) = update_Ymat(Y_now  = child_Y_i,
                                          i_axis = i_axis,
                                          Y_mat  = boxed_Y_mat[0],
                                          Y_1    = boxed_Y_1[0],
                                          Y_S    = boxed_Y_S[0],
                                          Y_L    = boxed_Y_L[0],
                                          B_S    = B_S,
                                          B_L    = B_L
                                         )
                #print("chkp_A") # DELETE THIS
                (child_P_i,
                 child_EP_i,
                 child_EP2_i,
                 child_varP_i) = update_stats(P_i    = boxed_P_i[0],
                                              EP_i   = boxed_EP_i[0],
                                              EP2_i  = boxed_EP2_i[0],
                                              varP_i = boxed_varP_i[0],
                                              Y_1    = child_Y_1,
                                              Y_S    = child_Y_S,
                                              Y_L    = child_Y_L,
                                              B_S    = B_S,
                                              B_L    = B_L
                                             )
                #print("chkp_B") # DELETE THIS
                
                (child_W_ABS,
                 child_W_i) = get_W_now(W_ABP       = W_ABP,
                                        W_ABS       = boxed_W_ABS[0],
                                        i_cot_a_i_6 = i_cot_a_i_6,
                                        HL_cot_a_i  = HL_cot_a_i,
                                        T_i_nml     = T_i_nml,
                                        P_i         = child_P_i,
                                        EP_i        = child_EP_i,
                                        varP_i      = child_varP_i,
                                        i_axis      = i_axis,
                                        D           = D,
                                        P_0         = P_0
                                       )
                child_w_l = get_w_now(boxed_w_l[0],
                                      l_axis,
                                      child_W_i,
                                      i_axis,
                                      M,
                                      L
                                     )
                if DEBUGGING:
                    assert len(child_Y_i)        == len(boxed_Y_i[0])
                    assert child_Y_i.dtype       == boxed_Y_i[0].dtype
                    assert len(child_W_i)        == len(boxed_W_i[0])
                    assert child_W_i.dtype       == boxed_W_i[0].dtype
                    assert len(child_w_l)        == len(boxed_w_l[0])
                    assert child_w_l.dtype       == boxed_w_l[0].dtype
                    assert np.shape(child_Y_mat) == np.shape(boxed_Y_mat[0])
                    assert child_Y_mat.dtype     == boxed_Y_mat[0].dtype
                    assert len(child_Y_1)        == len(boxed_Y_1[0])
                    assert child_Y_1.dtype       == boxed_Y_1[0].dtype
                    assert len(child_Y_S)        == len(boxed_Y_S[0])
                    assert child_Y_S.dtype       == boxed_Y_S[0].dtype
                    assert len(child_Y_L)        == len(boxed_Y_L[0])
                    assert child_Y_L.dtype       == boxed_Y_L[0].dtype
                    assert len(child_P_i)        == len(boxed_P_i[0])
                    assert child_P_i.dtype       == boxed_P_i[0].dtype
                    assert len(child_EP_i)       == len(boxed_EP_i[0])
                    assert child_EP_i.dtype      == boxed_EP_i[0].dtype
                    assert len(child_EP2_i)      == len(boxed_EP2_i[0])
                    assert child_EP2_i.dtype     == boxed_EP2_i[0].dtype
                    assert len(child_varP_i)     == len(boxed_varP_i[0])
                    assert child_varP_i.dtype    == boxed_varP_i[0].dtype
                    assert len(child_W_ABS)      == len(boxed_W_ABS[0])
                    assert child_W_ABS.dtype     == boxed_W_ABS[0].dtype
                    
                boxed_Y_i[0]    = np.array(child_Y_i)
                boxed_W_i[0]    = np.array(child_W_i)
                boxed_w_l[0]    = np.array(child_w_l)
                boxed_Y_mat[0]  = np.array(child_Y_mat)
                boxed_Y_1[0]    = np.array(child_Y_1)
                boxed_Y_S[0]    = np.array(child_Y_S)
                boxed_Y_L[0]    = np.array(child_Y_L)
                boxed_P_i[0]    = np.array(child_P_i)
                boxed_EP_i[0]   = np.array(child_EP_i)
                boxed_EP2_i[0]  = np.array(child_EP2_i)
                boxed_varP_i[0] = np.array(child_varP_i)
                boxed_W_ABS[0]  = np.array(child_W_ABS)
                
            except:
                PrintException()
                break
            
        else:
            time.sleep(0.0025)    
    
    print("AFB: exiting")
    return



# ~~ 6. Principal function ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---- 6a. Real-time plotting --------------------------------------------------

def RT_plot(pls_stop_plt,
            i_axis,
            multi_W_i,
            multi_P_i
           ):
    
    if DEBUGGING:
        assert            type(pls_stop_plt) == mp.synchronize.Event
        assert            type(i_axis)       == np.ndarray
        assert issubclass(type(i_axis[0])     , np.integer)
        assert            type(multi_W_i)    == mp.managers.ListProxy
        assert            type(multi_P_i)    == mp.managers.ListProxy
    
    fig, ax = plt.subplots(2)
    fig.tight_layout(pad=3.0)

    while not pls_stop_plt.wait(0):
        
        try:
            multi_P_i_cp = np.array(multi_P_i)
            ax[0].clear()
            ax[0].plot(i_axis, multi_P_i_cp, color="red", linewidth=4, alpha=0.5)
            ax[0].set_title('signal level (64 channels, 100 times $s^{-1}$)')
            ax[0].axvline(x = 32, linestyle = '--', color = 'gray')
            ax[0].set_yscale('log', base=2)
            ax[0].set_ylim([1, 3500])
            multi_W_i_cp = np.array(multi_W_i)
            ax[1].clear()
            ax[1].plot(i_axis, multi_W_i_cp, color="blue", linewidth=4, alpha=0.5)
            ax[1].set_ylim([0, 7])
            ax[1].set_title('spectral gains ($W_{ABP}$ x $W_{ABS}$ x $W_{FS}$)')
            plt.pause(0.05)
        except KeyboardInterrupt:
            break
    
    return

# ---- 6b. The main function ---------------------------------------------------
# see fbe_high_level_functions.py
