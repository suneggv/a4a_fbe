# ==============================================================================
# ==  2.1 IMPORTS  =============================================================
# ==============================================================================

# ~~ module access from main ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
sys.path.append('./FBEmodules')


# -- audio manipulation --------------------------------------------------------
import numpy as np
import math
from pydub import AudioSegment


# -- record / playback ---------------------------------------------------------
import pyaudio
import time


# -- DCT -----------------------------------------------------------------------
import scipy as sp


# -- sidechain -----------------------------------------------------------------
import threading


# -- visualization -------------------------------------------------------------
from debugging_and_visualization import VISUALIZATION
import matplotlib.pyplot as plt 
import scipy.signal as signal
from   scipy.fft import fft, fftfreq, fftshift, dct
import multiprocessing as mp


# -- debugging -----------------------------------------------------------------
from debugging_and_visualization import DEBUGGING
import numbers


# -- copying nparrays ----------------------------------------------------------
import copy


# ==============================================================================
# ==  3.1.1 PARAMETERS  ========================================================
# ==============================================================================

# -- manager -------------------------------------------------------------------

sleep_fraction = 0.2


# -- file addresses ------------------------------------------------------------

addr__A_k     = './freqShapDat/A_k.txt'
addr__T_i_nml = './freqShapDat/T_i_nml.txt'
addr__C_ik    = './freqShapDat/C_ik.txt'
addr__W_ABP   = './ABPdat/W_ABP.txt'


# -- pyaudio -------------------------------------------------------------------

pm__chunk__sam         = 1024            # Each chunk will be 1024 samples long
pm__sample_format__int = pyaudio.paInt16 # 16 bits per sample
pm__channels__int      = 2               # Number of audio channels
pm__fs__Hz             = 44100           # Record at 44100 samples per second


# -- FBE -----------------------------------------------------------------------

pm__M__int = 64  # number of channels
pm__L__int = 64  # length of filter
pm__R__sam = 441 # refresh samples 


# -- level detection -----------------------------------------------------------

pm__BS__int = 5   # short backlog (ie # of past Yi values to use for lvl detect)
pm__BL__int = 200 # long  backlog (ie # of past Yi values to use for statistics)


# -- adaptive band subtraction -------------------------------------------------

pm__decay__fct = 0.99


# -- frequency shaping ---------------------------------------------------------

pm__P_0__fct = 1.1082721367868039e-07 # lin-to-dB conversion reference value


# -- matplotlib.pyplot ---------------------------------------------------------

plt.rcParams["figure.figsize"] = (7,7)



# ==============================================================================
# ==  FUNCTION IMPORTS  ========================================================
# ==============================================================================

# == imports -- FBE_with_microphone ============================================
from FBE_with_microphone import mike_callback
from FBE_with_microphone import mike_queue_manager
from FBE_with_microphone import line_callback
from FBE_with_microphone import line_queue_manager
from FBE_with_microphone import FBE_with_microphone
from FBE_with_microphone import FBE_manager


# == imports -- high ===========================================================

# -- _______ -- ___ -- Import audio -  -  -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import import_audio # y(n)

# -- _______ -- ___ -- Constant FVV calculation -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import hann_window        # win_L(l)
from fbe_high_level_functions import prototype_function # h(l)

# -- _______ -- ___ -- Constant FVM calculation -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import modulation_sequence # varphi(i,l)

# -- _______ -- ___ -- Principal function -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import main_function # v(n)


# == imports -- mid ============================================================

# -- _______ -- ___ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import get_beta_now # beta_now(l)

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import AFB_sidechain # w_l^now for n^now

# -- _______ -- ____ -- Principal function -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import RT_plot # Real-time plotting


# == imports -- sunk ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import get_Y_now   # Y_i_now
from fbe_sunk_level_functions import update_Ymat # Y_mat, Y_1, Y_S, Y_L
from fbe_sunk_level_functions import get_W_now   # W_i_now
from fbe_sunk_level_functions import get_w_now   # w_l_now

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import update_stats # P_i,E(P_i),E(P_i^2),Var(P_i)


# == imports -- yawn ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_yawn_level_functions import get_P               # P_i
from fbe_yawn_level_functions import get_avgP            # E(P_i)
from fbe_yawn_level_functions import get_avgP2           # E(P_i^2)
from fbe_yawn_level_functions import get_varP            # Var(P_i)
from fbe_yawn_level_functions import spect_lin_to_dB_SPL # W_i_now
from fbe_yawn_level_functions import get_W_ABS           # W_i_now
from fbe_yawn_level_functions import get_W_FSh           # W_i_now

# == imports -- debug ==========================================================

from debug_funcs import PrintException



FBE_manager( addr__A_k              = addr__A_k,
             addr__T_i_nml          = addr__T_i_nml,
             addr__C_ik             = addr__C_ik,
             addr__W_ABP            = addr__W_ABP,
             pm__chunk__sam         = pm__chunk__sam,
             pm__sample_format__int = pm__sample_format__int,
             pm__channels__int      = pm__channels__int,
             pm__fs__Hz             = pm__fs__Hz,
             pm__M__int             = pm__M__int,
             pm__L__int             = pm__L__int,
             pm__R__sam             = pm__R__sam,
             pm__BS__int            = pm__BS__int,
             pm__BL__int            = pm__BL__int,
             pm__decay__fct         = pm__decay__fct,
             pm__P_0__fct           = pm__P_0__fct,
             sleep_fraction         = sleep_fraction
            )
