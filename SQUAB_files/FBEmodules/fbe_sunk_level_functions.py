from module_imports import *

# == imports -- yawn ===========================================================

# -- _______ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  -  -  -  -
from fbe_yawn_level_functions import get_P               # P_i
from fbe_yawn_level_functions import get_avgP            # E(P_i)
from fbe_yawn_level_functions import get_avgP2           # E(P_i^2)
from fbe_yawn_level_functions import get_varP            # Var(P_i)
from fbe_yawn_level_functions import spect_lin_to_dB_SPL # W_i_now
from fbe_yawn_level_functions import get_W_ABS           # W_i_now
from fbe_yawn_level_functions import get_W_FSh           # W_i_now



# ~~ 2.2 Import audio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# see fbe_high_level_functions.py



# ~~ 4.1.1. Constant function value vector calculation ~~~~~~~~~~~~~~~~~~~~~~~~~
# see fbe_high_level_functions.py



# ~~ 4.1.2. Constant function value matrix calculation ~~~~~~~~~~~~~~~~~~~~~~~~~
# see fbe_high_level_functions.py



# ~~ 4.2.1. Variable function value vector / matrix calculation ~~~~~~~~~~~~~~~~

# ---- 4.2.1a. beta_now(l) -----------------------------------------------------
# see fbe_mid_level_functions.py

# ---- 4.2.1b. Y_i_now ---------------------------------------------------------

def get_Y_now(beta_now,
              h_phi,
              i_axis,
              l_axis
             ):

    if DEBUGGING:        
        assert            type(beta_now)      == np.ndarray
        assert      isinstance(beta_now[0],      numbers.Number)
        assert            type(h_phi)         == np.ndarray
        assert             len(h_phi.shape)   == 2
        assert np.iscomplexobj(h_phi)
        assert            type(i_axis)        == np.ndarray
        assert issubclass(type(i_axis[0])      , np.integer)
        assert            type(l_axis)        == np.ndarray
        assert issubclass(type(l_axis[0])      , np.integer)
        assert                 h_phi.shape[0] == len(i_axis)
        assert                 h_phi.shape[1] == len(l_axis)

    beta_now_ext = np.outer(np.ones(len(i_axis)), beta_now).astype(beta_now.dtype)
    piece_multip = (beta_now_ext * h_phi).astype(h_phi.dtype)
    out          = np.sum(piece_multip, axis = 1).astype(h_phi.dtype)
    
    if DEBUGGING:
        assert type(out)      == np.ndarray
        assert len( out)      == len(i_axis)
        assert      out.dtype == h_phi.dtype

    return out

# ---- 4.2.1c. Y_mat, Y_1, Y_S, Y_L --------------------------------------------

def update_Ymat(Y_now,
                i_axis,
                Y_mat,
                Y_1,
                Y_S,
                Y_L,
                B_S,
                B_L
               ):
    if DEBUGGING:
        assert                type(Y_now)       == np.ndarray
        assert          isinstance(Y_now[0],       numbers.Number)
        assert     np.iscomplexobj(Y_now[0])
        assert                type(i_axis)      == np.ndarray
        assert     issubclass(type(i_axis[0])    , np.integer)
        assert                type(Y_mat)       == np.ndarray
        assert                 len(Y_mat.shape) == 2
        assert          isinstance(Y_mat[0,0],     numbers.Number)
        assert not issubclass(type(Y_mat),         np.integer)
        assert                type(Y_1)         == np.ndarray
        assert          isinstance(Y_1[0],         numbers.Number)
        assert not issubclass(type(Y_1),           np.integer)
        assert                type(Y_S)         == np.ndarray
        assert          isinstance(Y_S[0],         numbers.Number)
        assert not issubclass(type(Y_S),           np.integer)
        assert                type(Y_L)         == np.ndarray
        assert          isinstance(Y_L[0],         numbers.Number)
        assert not issubclass(type(Y_L),           np.integer)
        assert                type(B_S)         == int
        assert                type(B_L)         == int
        assert                     B_S           < B_L
        assert                 len(Y_now)       == len(i_axis)
        assert                 len(Y_mat[:,0])  == len(i_axis)
        assert                 len(Y_mat[0,:])  == B_L
        assert                 len(Y_1)         == len(i_axis)
        assert                 len(Y_S)         == len(i_axis)
        assert                 len(Y_L)         == len(i_axis)
        
    Y_1   = np.abs(Y_now)
    Y_mat = np.concatenate( ( Y_mat[:,1:], 
                              Y_1.reshape((len(i_axis),1))
                            )
                            , axis=1 
                          )
    Y_S = Y_mat[:,-B_S]
    Y_L = Y_mat[:,-B_L]
    
    out = (Y_mat, Y_1, Y_S, Y_L)
    
    if DEBUGGING:
        assert                type(out)          == tuple
        assert                 len(out)          == 4
        assert                type(out[0])       == np.ndarray
        assert                 len(out[0].shape) == 2
        assert          isinstance(out[0][0,0],     numbers.Number)
        assert not issubclass(type(out[0]),         np.integer)
        assert                 len(out[0][:,0])  == len(i_axis)
        assert                 len(out[0][0,:])  == B_L
        assert                type(out[1])       == np.ndarray
        assert          isinstance(out[1][0],       numbers.Number)
        assert not issubclass(type(out[1]),         np.integer)
        assert                 len(out[1])       == len(i_axis)
        assert                type(out[2])       == np.ndarray
        assert          isinstance(out[2][0],       numbers.Number)
        assert not issubclass(type(out[2]),         np.integer)
        assert                 len(out[2])       == len(i_axis)
        assert                type(out[3])       == np.ndarray
        assert          isinstance(out[3][0],       numbers.Number)
        assert not issubclass(type(out[3]),         np.integer)
        assert                 len(out[3])       == len(i_axis)
    
    return out

# ---- 4.2.1d. P_i -------------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1e. E(P_i) ----------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1f. E(P_i^2) --------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1g. Var(P_i) ------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1h. W_i_now ---------------------------------------------------------

# see also fbe_yawn_level_functions.py

def get_W_now(W_ABP,
              W_ABS,
              i_cot_a_i_6,
              HL_cot_a_i,
              T_i_nml,
              P_i,
              EP_i,
              varP_i,
              i_axis,
              D   = 0.99,
              P_0 = 1.1082721367868039e-07
             ):
    
    if DEBUGGING:
        assert                type(W_ABS)       == np.ndarray
        assert          isinstance(W_ABS[0],       numbers.Number)
        assert not issubclass(type(W_ABS),         np.integer)
        assert                type(W_ABP)       == np.ndarray
        assert          isinstance(W_ABP[0],       numbers.Number)
        assert not issubclass(type(W_ABP),         np.integer)
        assert                type(i_cot_a_i_6) == np.ndarray
        assert          isinstance(i_cot_a_i_6[0], numbers.Number)
        assert not issubclass(type(i_cot_a_i_6),   np.integer)
        assert                type(HL_cot_a_i)  == np.ndarray
        assert          isinstance(HL_cot_a_i[0],  numbers.Number)
        assert not issubclass(type(HL_cot_a_i),    np.integer)
        assert                type(T_i_nml)     == np.ndarray
        assert          isinstance(T_i_nml[0],     numbers.Number)
        assert not issubclass(type(T_i_nml),       np.integer)
        assert                type(P_i)         == np.ndarray
        assert          isinstance(P_i[0],         numbers.Number)
        assert not issubclass(type(P_i),           np.integer)
        assert                type(EP_i)        == np.ndarray
        assert          isinstance(EP_i[0],        numbers.Number)
        assert not issubclass(type(EP_i),          np.integer)
        assert                type(varP_i)      == np.ndarray
        assert          isinstance(varP_i[0],      numbers.Number)
        assert not issubclass(type(varP_i),        np.integer)
        assert                type(i_axis)      == np.ndarray
        assert     issubclass(type(i_axis[0]),     np.integer)
        assert                 len(W_ABP)       == len(i_axis)
        assert                 len(i_cot_a_i_6) == len(i_axis)
        assert                 len(HL_cot_a_i)  == len(i_axis)
        assert                 len(T_i_nml)     == len(i_axis)
        assert                 len(P_i)         == len(i_axis)
        assert                 len(EP_i)        == len(i_axis)
        assert                 len(varP_i)      == len(i_axis)
        assert                type(D)           == float
        assert                type(P_0)         == float

    W_ABS = get_W_ABS(W_ABS,
                      P_i,
                      EP_i,
                      i_axis,
                      D
                     )
    W_FSh = get_W_FSh(i_cot_a_i_6,
                      HL_cot_a_i,
                      T_i_nml,
                      EP_i,
                      i_axis,
                      P_0
                     )
    W_now = W_ABP * W_ABS * W_FSh
    
    out = (W_ABS, W_now)
    
    if DEBUGGING:
        assert                type(out)
        assert                type(out[0]) == np.ndarray      # W_ABS
        assert          isinstance(out[0][0], numbers.Number)
        assert not issubclass(type(out[0]),   np.integer)
        assert                 len(out[0]) == len(i_axis)
        assert                type(out[1]) == np.ndarray      # W_now
        assert          isinstance(out[1][0], numbers.Number)
        assert not issubclass(type(out[1]),   np.integer)
        assert                 len(out[1]) == len(i_axis)
        
    return out

# ---- 4.2.1i. w_l_now ---------------------------------------------------------

def get_w_now(w_old,
              l_axis,
              W_now,
              i_axis,
              M = 64,
              L = 64
             ):
    
    if DEBUGGING:
        assert                type(w_old)    == np.ndarray
        assert          isinstance(w_old[0],    numbers.Number)
        assert not issubclass(type(w_old),      np.integer)
        assert                type(l_axis)   == np.ndarray
        assert     issubclass(type(l_axis[0]) , np.integer)
        assert                 len(w_old)    == len(l_axis)
        assert                type(W_now)    == np.ndarray
        assert          isinstance(W_now[0],    numbers.Number)
        assert not issubclass(type(W_now),      np.integer)
        assert                type(i_axis)   == np.ndarray
        assert     issubclass(type(i_axis[0]) , np.integer)
        assert                 len(W_now)    == len(i_axis)
        assert                type(M)        == int
        assert                     M % 2     == 0
        assert                     M         == L
        
    w_now_k_short = 0.508 * dct(W_now,   # scipy.fft.dct yields output the same
                                type = 1 # size as the input. We need to add 
                               )         # one element at the end
    w_now_short = np.roll(w_now_k_short, int(L/2)) # w_{l} = w_{k + L/2}
    w_now = np.append(w_now_short, w_now_short[0]).astype(w_old.dtype) 
                                                   # w_{l=0} == w_{l=M}
    if DEBUGGING:
        assert     w_now.dtype == w_old.dtype
        assert len(w_now)      == len(l_axis)
    
    return w_now



# ~~ 5. Analysis filter bank ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---- 5a. Update the value of P_i, E(P_i), E(P_i^2) and Var(P_i) --------------

def update_stats(P_i,
                 EP_i,
                 EP2_i,
                 varP_i,
                 Y_1,
                 Y_S,
                 Y_L,
                 B_S,
                 B_L
                ):

    if DEBUGGING:
        assert                type(P_i)      == np.ndarray
        assert          isinstance(P_i[0],      numbers.Number)
        assert not issubclass(type(P_i),        np.integer)
        assert                type(EP_i)     == np.ndarray
        assert          isinstance(EP_i[0],     numbers.Number)
        assert not issubclass(type(EP_i),       np.integer)
        assert                type(EP2_i)    == np.ndarray
        assert          isinstance(EP2_i[0],    numbers.Number)
        assert not issubclass(type(EP2_i),      np.integer)
        assert                type(varP_i)   == np.ndarray
        assert          isinstance(varP_i[0],   numbers.Number)
        assert not issubclass(type(varP_i),     np.integer)
        assert                type(Y_1)      == np.ndarray
        assert          isinstance(Y_1[0],      numbers.Number)
        assert not issubclass(type(Y_1),        np.integer)
        assert                type(Y_S)      == np.ndarray
        assert          isinstance(Y_S[0],      numbers.Number)
        assert not issubclass(type(Y_S),        np.integer)
        assert                type(Y_L)      == np.ndarray
        assert          isinstance(Y_L[0],      numbers.Number)
        assert not issubclass(type(Y_L),        np.integer)
        assert                type(B_S)      == int
        assert                type(B_L)      == int
        assert                 len(P_i)      == len(Y_1)
        assert                 len(P_i)      == len(Y_S)
        assert                 len(P_i)      == len(EP_i)
        assert                 len(EP_i)     == len(Y_L)
        assert                 len(EP_i)     == len(EP2_i)
        assert                 len(varP_i)   == len(EP_i)
    
    P_i = get_P(P_i,
                Y_1,
                Y_S,
                B_S
               )
    EP_i = get_avgP(EP_i,
                    Y_1,
                    Y_L,
                    B_L
                   )
    EP2_i = get_avgP2(EP2_i,
                      Y_1,
                      Y_L,
                      B_L
                     )
    varP_i = get_varP(EP_i,
                      EP2_i
                     )
    out = (P_i, EP_i, EP2_i, varP_i)
    
    if DEBUGGING:
        assert                type(out)    == tuple
        assert                 len(out)    == 4
        assert                type(out[0]) == np.ndarray
        assert          isinstance(out[0][0], numbers.Number)
        assert not issubclass(type(out[0]),   np.integer)
        assert                type(out[1]) == np.ndarray
        assert          isinstance(out[1][0], numbers.Number)
        assert not issubclass(type(out[1]),   np.integer)
        assert                type(out[2]) == np.ndarray
        assert          isinstance(out[2][0], numbers.Number)
        assert not issubclass(type(out[2]),   np.integer)
        assert                type(out[3]) == np.ndarray
        assert          isinstance(out[3][0], numbers.Number)
        assert not issubclass(type(out[3]),   np.integer)
    
    return out

# ---- 5b. Update the value of w_l^now for n^now -------------------------------
# see fbe_mid_level_functions.py



# ~~ 6. Principal function ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---- 6a. Real-time plotting --------------------------------------------------
# see fbe_mid_level_functions.py

# ---- 6b. The main function ---------------------------------------------------
# see fbe_high_level_functions.py
