from module_imports import *

# ~~ 2.2 Import audio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# see fbe_high_level_functions.py



# ~~ 4.1.1. Constant function value vector calculation ~~~~~~~~~~~~~~~~~~~~~~~~~
# see fbe_high_level_functions.py



# ~~ 4.1.2. Constant function value matrix calculation ~~~~~~~~~~~~~~~~~~~~~~~~~
# see fbe_high_level_functions.py



# ~~ 4.2.1. Variable function value vector / matrix calculation ~~~~~~~~~~~~~~~~

# ---- 4.2.1d. P_i -------------------------------------------------------------

def get_P(P_i,
          Y_1,
          Y_S,
          B_S
         ):
    
    if DEBUGGING:
        assert                type(P_i) == np.ndarray
        assert          isinstance(P_i[0], numbers.Number)
        assert not issubclass(type(P_i),   np.integer)
        assert                type(Y_1) == np.ndarray
        assert          isinstance(Y_1[0], numbers.Number)
        assert not issubclass(type(Y_1),   np.integer)
        assert                type(Y_S) == np.ndarray
        assert          isinstance(Y_S[0], numbers.Number)
        assert not issubclass(type(Y_S),   np.integer)
        assert                type(B_S) == int
        assert                 len(P_i) == len(Y_1)
        assert                 len(P_i) == len(Y_S)
    
    new_P_i = P_i + (1.0/B_S)*(Y_1 - Y_S)
    
    if DEBUGGING:
        assert type(new_P_i) == type(P_i)
        assert new_P_i.dtype == P_i.dtype
        
    return new_P_i

# ---- 4.2.1e. E(P_i) ----------------------------------------------------------

def get_avgP(EP_i,
             Y_1,
             Y_L,
             B_L
            ):
    
    if DEBUGGING:
        assert                type(EP_i) == np.ndarray
        assert          isinstance(EP_i[0], numbers.Number)
        assert not issubclass(type(EP_i),   np.integer)
        assert                type(Y_1)  == np.ndarray
        assert          isinstance(Y_1[0],  numbers.Number)
        assert not issubclass(type(Y_1),    np.integer)
        assert                type(Y_L)  == np.ndarray
        assert          isinstance(Y_L[0],  numbers.Number)
        assert not issubclass(type(Y_L),    np.integer)
        assert                type(B_L)  == int
        assert                 len(EP_i) == len(Y_1)
        assert                 len(EP_i) == len(Y_L)
    
    new_EP_i = EP_i + (1.0/B_L)*(Y_1 - Y_L)
    
    if DEBUGGING:
        assert type(new_EP_i) == type(EP_i)
        assert new_EP_i.dtype == EP_i.dtype
        
    return new_EP_i

# ---- 4.2.1f. E(P_i^2) --------------------------------------------------------

def get_avgP2(EP2_i,
              Y_1,
              Y_L,
              B_L
             ):
    
    if DEBUGGING:
        assert                type(EP2_i) == np.ndarray
        assert          isinstance(EP2_i[0], numbers.Number)
        assert not issubclass(type(EP2_i),   np.integer)
        assert                type(Y_1)   == np.ndarray
        assert          isinstance(Y_1[0],   numbers.Number)
        assert not issubclass(type(Y_1),     np.integer)
        assert                type(Y_L)   == np.ndarray
        assert          isinstance(Y_L[0],   numbers.Number)
        assert not issubclass(type(Y_L),     np.integer)
        assert                type(B_L)   == int
        assert                 len(EP2_i) == len(Y_1)
        assert                 len(EP2_i) == len(Y_L)
    
    new_EP2_i = EP2_i + (1.0/B_L)*(Y_1**2 - Y_L**2)
    
    if DEBUGGING:
        assert type(new_EP2_i) == type(EP2_i)
        assert new_EP2_i.dtype == EP2_i.dtype
        
    return new_EP2_i

# ---- 4.2.1g. Var(P_i) ------------------------------------------------------

def get_varP(EP_i,
             EP2_i
            ):
    
    if DEBUGGING:
        assert                type(EP_i)  == np.ndarray
        assert          isinstance(EP_i[0],  numbers.Number)
        assert not issubclass(type(EP_i),    np.integer)
        assert                type(EP2_i) == np.ndarray
        assert          isinstance(EP2_i[0], numbers.Number)
        assert not issubclass(type(EP2_i),   np.integer)
    
    new_varP = EP2_i - EP_i**2
    
    if DEBUGGING:
        assert                type(new_varP) == np.ndarray
        assert          isinstance(new_varP[0], numbers.Number)
        assert not issubclass(type(new_varP),   np.integer)
    
    return new_varP

# ---- 4.2.1h. W_i_now ---------------------------------------------------------

def spect_lin_to_dB_SPL(EP_i,
                        i_axis,
                        P_0 = 1.1082721367868039e-07
                       ):
    
    if DEBUGGING:
        assert                type(EP_i)    == np.ndarray
        assert          isinstance(EP_i[0],    numbers.Number)
        assert not issubclass(type(EP_i),      np.integer)
        assert                type(i_axis)  == np.ndarray
        assert     issubclass(type(i_axis[0]), np.integer)
        assert                 len(EP_i)    == len(i_axis)
        assert                type(P_0)     == float
    
    EP_i_dB               = np.zeros(len(EP_i))
    nonzero_idxs          = [i for i, v_i in enumerate(EP_i) if v_i > 10]
    EP_i_dB[nonzero_idxs] = 10 * np.log10(EP_i[nonzero_idxs]/P_0)

    if DEBUGGING:
        assert                type(EP_i_dB) == np.ndarray
        assert          isinstance(EP_i_dB[0], numbers.Number)
        assert             np.amin(EP_i_dB) >= 0.0
        assert             np.amax(EP_i_dB)  < 109.0
    
    return EP_i_dB


def get_W_ABS(W_ABS,
              P_i,
              EP_i,
              i_axis,
              D = 0.99
             ):
    if DEBUGGING:
        assert                type(W_ABS)   == np.ndarray
        assert          isinstance(W_ABS[0],   numbers.Number)
        assert not issubclass(type(W_ABS),     np.integer)
        assert                type(P_i)     == np.ndarray
        assert          isinstance(P_i[0],     numbers.Number)
        assert not issubclass(type(P_i),       np.integer)
        assert                type(EP_i)    == np.ndarray
        assert          isinstance(EP_i[0],    numbers.Number)
        assert not issubclass(type(EP_i),      np.integer)
        assert                type(i_axis)  == np.ndarray
        assert     issubclass(type(i_axis[0]), np.integer)
        assert                 len(P_i)     == len(i_axis)
        assert                 len(EP_i)    == len(i_axis)
        assert                 len(W_ABS)   == len(i_axis)
        assert                type(D)       == float
    
    xi_i      = 10*(P_i - EP_i)/(P_i + 1)
    new_W_ABS = np.maximum(np.maximum(D*W_ABS,xi_i),0.125)
    
    if DEBUGGING:
        assert                type(new_W_ABS)   == np.ndarray
        assert          isinstance(new_W_ABS[0],   numbers.Number)
        assert not issubclass(type(new_W_ABS),     np.integer)
        assert                 len(new_W_ABS)   == len(i_axis)
    
    return new_W_ABS


def get_W_FSh(i_cot_a_i_6,
              HL_cot_a_i,
              T_i_nml,
              EP_i,
              i_axis,
              P_0 = 1.1082721367868039e-07
             ):
    
    if DEBUGGING:
        assert                type(i_cot_a_i_6) == np.ndarray
        assert          isinstance(i_cot_a_i_6[0], numbers.Number)
        assert not issubclass(type(i_cot_a_i_6),   np.integer)
        assert                type(HL_cot_a_i)  == np.ndarray
        assert          isinstance(HL_cot_a_i[0],  numbers.Number)
        assert not issubclass(type(HL_cot_a_i),    np.integer)
        assert                type(T_i_nml)     == np.ndarray
        assert          isinstance(T_i_nml[0],     numbers.Number)
        assert not issubclass(type(T_i_nml),       np.integer)
        assert                type(EP_i)        == np.ndarray
        assert          isinstance(EP_i[0],        numbers.Number)
        assert not issubclass(type(EP_i),          np.integer)
        assert                type(i_axis)      == np.ndarray
        assert     issubclass(type(i_axis[0]),     np.integer)
        assert                 len(i_cot_a_i_6) == len(i_axis)
        assert                 len(HL_cot_a_i)  == len(i_axis)
        assert                 len(T_i_nml)     == len(i_axis)
        assert                 len(EP_i)        == len(i_axis)
        assert                type(P_0)         == float

    EP_i_dB  = spect_lin_to_dB_SPL(EP_i, i_axis, P_0)
    diff_dB  = HL_cot_a_i + T_i_nml - EP_i_dB
    Guh_i    = i_cot_a_i_6 * diff_dB
    W_unnorm = np.power(2*np.ones(len(i_axis)), Guh_i)
    W_FSh    = len(i_axis) * W_unnorm / np.sum(W_unnorm)
    
    if DEBUGGING:
        assert                type(W_FSh) == np.ndarray
        assert          isinstance(W_FSh[0], numbers.Number)
        assert not issubclass(type(W_FSh),   np.integer)
        assert                 len(W_FSh) == len(i_axis)
        
    return W_FSh

# see also fbe_sunk_level_functions.py

# ---- 4.2.1i. w_l_now ---------------------------------------------------------
# see fbe_sunk_level_functions.py



# ~~ 5. Analysis filter bank ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---- 5a. Update the value of P_i, E(P_i), E(P_i^2) and Var(P_i) --------------
# see fbe_sunk_level_functions.py

# ---- 5b. Update the value of w_l^now for n^now -------------------------------
# see fbe_mid_level_functions.py



# ~~ 6. Principal function ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---- 6a. Real-time plotting --------------------------------------------------
# see fbe_mid_level_functions.py

# ---- 6b. The main function ---------------------------------------------------
# see fbe_high_level_functions.py
