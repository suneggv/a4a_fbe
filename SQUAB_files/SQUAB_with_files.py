# ==============================================================================
# ==  2.1 IMPORTS  =============================================================
# ==============================================================================

# ~~ module access from main ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
sys.path.append('./FBEmodules')

# -- audio manipulation --------------------------------------------------------
import numpy as np
import math
from pydub import AudioSegment


# -- record / playback ---------------------------------------------------------
import pyaudio
import time


# -- DCT -----------------------------------------------------------------------
import scipy as sp


# -- sidechain -----------------------------------------------------------------
import threading


# -- visualization -------------------------------------------------------------
from debugging_and_visualization import VISUALIZATION
import matplotlib.pyplot as plt 
import scipy.signal as signal
from   scipy.fft import fft, fftfreq, fftshift, dct
import multiprocessing as mp

# -- debugging -----------------------------------------------------------------
from debugging_and_visualization import DEBUGGING
import numbers



# ==============================================================================
# ==  3.1.1 PARAMETERS  ========================================================
# ==============================================================================

# -- file addresses ------------------------------------------------------------

addr__file_name    = "example_audio.mp3"
the_file_is_stereo = True

addr__A_k     = './freqShapDat/A_k.txt'
addr__T_i_nml = './freqShapDat/T_i_nml.txt'
addr__C_ik    = './freqShapDat/C_ik.txt'
addr__W_ABP   = './ABPdat/W_ABP.txt'


# -- pyaudio -------------------------------------------------------------------

pm__chunk__sam         = 1024            # Each chunk will be 1024 samples long
pm__sample_format__int = pyaudio.paInt16 # 16 bits per sample
pm__channels__int      = 2               # Number of audio channels
pm__fs__Hz             = 44100           # Record at 44100 samples per second


# -- FBE -----------------------------------------------------------------------

pm__M__int = 64  # number of channels
pm__L__int = 64  # length of filter
pm__R__sam = 441 # refresh samples 


# -- level detection -----------------------------------------------------------

pm__BS__int = 5   # short backlog (ie # of past Yi values to use for lvl detect)
pm__BL__int = 200 # long  backlog (ie # of past Yi values to use for statistics)


# -- adaptive band subtraction -------------------------------------------------

pm__decay__fct = 0.99


# -- frequency shaping ---------------------------------------------------------

pm__P_0__fct = 1.1082721367868039e-07 # lin-to-dB conversion reference value


# -- matplotlib.pyplot ---------------------------------------------------------

plt.rcParams["figure.figsize"] = (9,3)



# ==============================================================================
# ==  FUNCTION IMPORTS  ========================================================
# ==============================================================================

# == imports -- high ============================================================

# -- _______ -- ___ -- Import audio -  -  -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import import_audio # y(n)

# -- _______ -- ___ -- Constant FVV calculation -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import hann_window        # win_L(l)
from fbe_high_level_functions import prototype_function # h(l)

# -- _______ -- ___ -- Constant FVM calculation -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import modulation_sequence # varphi(i,l)

# -- _______ -- ___ -- Principal function -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import main_function # v(n)


# == imports -- mid ============================================================

# -- _______ -- ___ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import get_beta_now # beta_now(l)

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import AFB_sidechain # w_l^now for n^now

# -- _______ -- ____ -- Principal function -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import RT_plot # Real-time plotting


# == imports -- sunk ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import get_Y_now   # Y_i_now
from fbe_sunk_level_functions import update_Ymat # Y_mat, Y_1, Y_S, Y_L
from fbe_sunk_level_functions import get_W_now   # W_i_now
from fbe_sunk_level_functions import get_w_now   # w_l_now

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import update_stats # P_i,E(P_i),E(P_i^2),Var(P_i)


# == imports -- yawn ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_yawn_level_functions import get_P               # P_i
from fbe_yawn_level_functions import get_avgP            # E(P_i)
from fbe_yawn_level_functions import get_avgP2           # E(P_i^2)
from fbe_yawn_level_functions import get_varP            # Var(P_i)
from fbe_yawn_level_functions import spect_lin_to_dB_SPL # W_i_now
from fbe_yawn_level_functions import get_W_ABS           # W_i_now
from fbe_yawn_level_functions import get_W_FSh           # W_i_now

# == imports -- debug ==========================================================

from debug_funcs import PrintException



def FBE_with_files( addr__file_name,
                    the_file_is_stereo,
                    addr__A_k,
                    addr__T_i_nml,
                    addr__C_ik,
                    addr__W_ABP,
                    pm__chunk__sam         = 1024,
                    pm__sample_format__int = pyaudio.paInt16,
                    pm__channels__int      = 2,
                    pm__fs__Hz             = 44100,
                    pm__M__int             = 64,
                    pm__L__int             = 64,
                    pm__R__sam             = 441,
                    pm__BS__int            = 5,
                    pm__BL__int            = 200,
                    pm__decay__fct         = 0.99,
                    pm__P_0__fct           = 1.1082721367868039e-07
                  ):

    if DEBUGGING:
        assert type(addr__file_name)        == str
        assert type(the_file_is_stereo)     == bool
        assert type(addr__A_k)              == str
        assert type(addr__T_i_nml)          == str
        assert type(addr__C_ik)             == str
        assert type(addr__W_ABP)            == str
        assert type(pm__chunk__sam)         == int
        assert type(pm__sample_format__int) == int
        assert type(pm__channels__int)      == int
        assert type(pm__fs__Hz)             == int
        assert type(pm__M__int)             == int
        assert type(pm__L__int)             == int
        assert type(pm__R__sam)             == int
        assert type(pm__BS__int)            == int
        assert type(pm__BL__int)            == int
        assert type(pm__decay__fct)         == float
        assert type(pm__P_0__fct)           == float

    # 2. IMPORTS ---------------------------------------------------------------

    # -- 2.1. Import modules - - - - - - - - - - - - - - - - - - - - - - - - - -

    # see above

    # -- 2.2. Import audio - - - - - - - - - - - - - - - - - - - - - - - - - - -

    vs__yL__int = import_audio( file_name          = addr__file_name,
                                the_file_is_stereo = the_file_is_stereo
                              )

    # -- 2.3. Import frequency shaper data - - - - - - - - - - - - - - - - - - -

    vv__A_k__dBH     = np.loadtxt(addr__A_k)
    vv__T_i_nml__dBS = np.loadtxt(addr__T_i_nml)
    mf__C_ik__fct    = np.loadtxt(addr__C_ik)
    vv__W_ABP__fct   = np.loadtxt(addr__W_ABP)


    # 3. Settings, definitions and instantiations ------------------------------

    # ---- 3.1.1. Set parameter values -  -  -  -  -  -  -  -  -  -  -  -  -  - 

    # see above

    # ---- 3.1.2. Set signal value vectors -  -  -  -  -  -  -  -  -  -  -  -  -

    vs__y__int = vs__yL__int # in units of samples

    # ---- 3.2.1. Define axes and coords -  -  -  -  -  -  -  -  -  -  -  -  -  

    # ------ 3.2.1a. axes -   -   -   -   -   -   -   -   -   -   -   -   -   - 

    va__n__sam = np.array(range(len(vs__y__int)))
    va__t__sec = va__n__sam / pm__fs__Hz
    va__l__idx = np.array(range(pm__L__int + 1))
    va__i__idx = np.array(range(pm__M__int))
    va__k__idx = np.array(range(8))

    # ------ 3.2.1b. coords -   -   -   -   -   -   -   -   -   -   -   -   -   

    pc__n_now__idx = 0

    # ---- 3.3.1. Instantiate SVVs, FVVs and FVMs -  -  -  -  -  -  -  -  -  -  

    # ------ 3.3.1a. single value vectors (SVVs) -   -   -   -   -   -   -   -  

    vs__beta__int = np.zeros(len(va__l__idx)).astype(vs__y__int.dtype)
    vs__v__int    = np.zeros(len(va__n__sam)).astype(vs__y__int.dtype)


    # ------ 3.3.1b. function value vectors (FVVs) -   -   -   -   -   -   -   -

    vf__win__flt = np.zeros(len(va__l__idx))
    vf__h__flt   = np.zeros(len(va__l__idx))

    vf__Y__flt  = np.zeros(len(va__i__idx)).astype("complex128")
    vf__Y1__flt = np.zeros(len(va__i__idx)).astype("float64")
    vf__YS__flt = np.zeros(len(va__i__idx)).astype("float64")
    vf__YL__flt = np.zeros(len(va__i__idx)).astype("float64")

    vv__W_ABS__fct = np.zeros(len(va__i__idx))
    vf__W__fct     = np.zeros(len(va__i__idx))
    vf__w__flt     = np.zeros(len(va__l__idx))

    vf__P__flt     = np.zeros(len(va__i__idx))
    vf__avgP__flt  = np.zeros(len(va__i__idx))
    vf__avgP2__flt = np.zeros(len(va__i__idx))
    vf__varP__flt  = np.zeros(len(va__i__idx))

    vf__HL_i__dB_        = np.zeros(len(va__i__idx))
    vf__a_i__rad         = np.zeros(len(va__i__idx))
    vf__1_cot_a_i__fct   = np.zeros(len(va__i__idx))
    vf__1_cot_a_i_6__fct = np.zeros(len(va__i__idx))
    vf__HL_cot_a_i__dB_  = np.zeros(len(va__i__idx))


    # ------ 3.3.1c. function value matrices (FVMs) -   -   -   -   -   -   -   

    mf__varphi__xfl = np.zeros((len(va__i__idx),
                                len(va__l__idx))).astype("complex128")

    mf__h_varphi__xfl = np.zeros((len(va__i__idx),
                                  len(va__l__idx))).astype("complex128")

    mf__Y__flt = np.zeros( (len(va__i__idx), pm__BL__int) ).astype("float64")


    # 4. Function value thing salad calculation --------------------------------

    # ---- 4.1.1. Constant function value vector calculation -  -  -  -  -  -  -

    # ~~~~~~ 4.1.1a. win_L(l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for l in va__l__idx:
        vf__win__flt[l] = hann_window(l, pm__L__int)


    # ~~~~~~ 4.1.1b. h(l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for l in va__l__idx:
        vf__h__flt[l] = prototype_function(l, 
                                           vf__win__flt, 
                                           pm__L__int, 
                                           pm__M__int)

        
    # ~~~~~~ 4.1.1c. HL_i ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for i in va__i__idx:
        vf__HL_i__dB_[i] = np.dot(vv__A_k__dBH, mf__C_ik__fct[i,:])


    # ~~~~~~ 4.1.1d. alpha_i ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    for i in va__i__idx:
        vf__a_i__rad[i] = (47.0 + 0.45 * vf__HL_i__dB_[i]) / 57.295779513


    # ~~~~~~ 4.1.1e. 1 - cot(alpha_i) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for i in va__i__idx:
        vf__1_cot_a_i__fct[i] = 1.0 - 1.0/np.tan(vf__a_i__rad[i])

        
    # ~~~~~~ 4.1.1f. (1 - cot(alpha_i)) / 6 ~   ~   ~   ~   ~   ~   ~   ~   ~   
    for i in va__i__idx:
        vf__1_cot_a_i_6__fct[i] = (1.0/6.0) * vf__1_cot_a_i__fct[i]
        

    # ~~~~~~ 4.1.1g. HL_i/(1 - cot(alpha_i)) ~   ~   ~   ~   ~   ~   ~   ~   ~  
    for i in va__i__idx:
        vf__HL_cot_a_i__dB_[i] = vf__HL_i__dB_[i]/vf__1_cot_a_i__fct[i]


    # ---- 4.1.2. Constant function value matrix calculation -  -  -  -  -  -  -

    # ~~~~~~ 4.1.2a. phi(i,l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    for i in va__i__idx:
        for l in va__l__idx:
            mf__varphi__xfl[i,l] = modulation_sequence(i,l,pm__M__int)


    # ~~~~~~ 4.1.2b. h(l)phi(i,l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    mf__h_ext__flt    = np.outer(np.ones(len(va__i__idx)), 
                                 vf__h__flt).astype(vf__h__flt.dtype)
    mf__h_varphi__xfl = mf__h_ext__flt * mf__varphi__xfl


    # ---- 4.2.1. Variable FVV/M calculation -  -  -  -  -  -  -  -  -  -  -  - 

    # ~~~~~~ 4.2.1a. beta_now(l) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    vs__beta__int = get_beta_now(vs__y__int,
                                 pc__n_now__idx,
                                 va__l__idx
                                )


    # ~~~~~~ 4.2.1b. Y_i_now ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    vf__Y__flt = get_Y_now(vs__beta__int,
                           mf__h_varphi__xfl,
                           va__i__idx,
                           va__l__idx
                          )


    # ~~~~~~ 4.2.1c. Y_mat, Y_1, Y_S, Y_L ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    (mf__Y__flt, 
     vf__Y1__flt,
     vf__YS__flt, 
     vf__YL__flt) = update_Ymat(Y_now  = vf__Y__flt,
                                i_axis = va__i__idx,
                                Y_mat  = mf__Y__flt,
                                Y_1    = vf__Y1__flt,
                                Y_S    = vf__YS__flt,
                                Y_L    = vf__YL__flt,
                                B_S    = pm__BS__int,
                                B_L    = pm__BL__int
                               )


    # ~~~~~~ 4.2.1d. P_i ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    vf__P__flt = get_P(P_i = vf__P__flt,
                       Y_1 = vf__Y1__flt,
                       Y_S = vf__YS__flt,
                       B_S = pm__BS__int
                      )


    # ~~~~~~ 4.2.1e. E(P_i) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   
    vf__avgP__flt = get_avgP(EP_i = vf__avgP__flt,
                             Y_1  = vf__Y1__flt,
                             Y_L  = vf__YL__flt,
                             B_L  = pm__BL__int
                            )


    # ~~~~~~ 4.2.1f. E(P_i^2) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~ 
    vf__avgP2__flt = get_avgP2(EP2_i = vf__avgP2__flt,
                               Y_1   = vf__Y1__flt,
                               Y_L   = vf__YL__flt,
                               B_L   = pm__BL__int
                              )


    # ~~~~~~ 4.2.1g. Var(P_i^2) ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   
    vf__varP__flt = get_varP(EP_i  = vf__avgP__flt,
                             EP2_i = vf__avgP2__flt
                            )


    # ~~~~~~ 4.2.1h. W_i_now ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    (vv__W_ABS__fct,
     vf__W__fct)     = get_W_now(W_ABP       = vv__W_ABP__fct,
                                 W_ABS       = vv__W_ABS__fct,
                                 i_cot_a_i_6 = vf__1_cot_a_i_6__fct,
                                 HL_cot_a_i  = vf__HL_cot_a_i__dB_,
                                 T_i_nml     = vv__T_i_nml__dBS,
                                 P_i         = vf__P__flt,
                                 EP_i        = vf__avgP__flt,
                                 varP_i      = vf__varP__flt,
                                 i_axis      = va__i__idx,
                                 D           = pm__decay__fct,
                                 P_0         = pm__P_0__fct
                                )


    # ~~~~~~ 4.2.1i. w_l_now ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~   ~  
    vf__w__flt = get_w_now(vf__w__flt,
                           va__l__idx,
                           vf__W__fct,
                           va__i__idx,
                           pm__M__int,
                           pm__L__int
                          )


    # 5. Analysis filter bank --------------------------------------------------

    # ~~ 5a. Update P_i, E(P_i), E(P_i^2) and Var(P_i) ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

    # used by the principal function

    # ~~ 5b. Update the value of w_l^now for n^now ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

    # used by the principal function


    # 6. Principal function ----------------------------------------------------

    vs__v__int = main_function(y              = vs__y__int,
                               v              = vs__v__int,
                               monad_beta_l   = vs__beta__int,
                               h_l            = vf__h__flt,
                               monad_w_l      = vf__w__flt,
                               monad_W_i      = vf__W__fct,
                               monad_Y_i      = vf__Y__flt,
                               h_phi          = mf__h_varphi__xfl,
                               n_axis         = va__n__sam,
                               i_axis         = va__i__idx,
                               l_axis         = va__l__idx,
                               monad_n_now    = pc__n_now__idx,
                               monad_Y_mat    = mf__Y__flt,
                               monad_Y_1      = vf__Y1__flt,
                               monad_Y_S      = vf__YS__flt,
                               monad_Y_L      = vf__YL__flt,
                               monad_P_i      = vf__P__flt,
                               monad_EP_i     = vf__avgP__flt,
                               monad_EP2_i    = vf__avgP2__flt,
                               monad_varP_i   = vf__varP__flt,
                               i_cot_a_i_6    = vf__1_cot_a_i_6__fct,
                               HL_cot_a_i     = vf__HL_cot_a_i__dB_,
                               T_i_nml        = vv__T_i_nml__dBS,
                               monad_W_ABS    = vv__W_ABS__fct,
                               W_ABP          = vv__W_ABP__fct,
                               D              = pm__decay__fct,
                               P_0            = pm__P_0__fct,
                               M              = pm__M__int,
                               L              = pm__L__int,
                               R              = pm__R__sam,
                               B_S            = pm__BS__int,
                               B_L            = pm__BL__int,
                               PLS_GIVE_PLOTS = False
                              )


    # 7. Export the result -----------------------------------------------------

    audio_title = "example_output.mp3"

    output_audio = AudioSegment(
                                vs__v__int.tobytes(),
                                frame_rate   = pm__fs__Hz,
                                sample_width = vs__v__int.dtype.itemsize,
                                channels     = 1
                               )

    output_audio.export(audio_title, 
                        format  = "mp3", 
                        bitrate = "128k"
                       )


FBE_with_files( addr__file_name,
                the_file_is_stereo,
                addr__A_k,
                addr__T_i_nml,
                addr__C_ik,
                addr__W_ABP,
                pm__chunk__sam,
                pm__sample_format__int,
                pm__channels__int,
                pm__fs__Hz,
                pm__M__int,
                pm__L__int,
                pm__R__sam,
                pm__BS__int,
                pm__BL__int,
                pm__decay__fct,
                pm__P_0__fct
              )
